﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;

namespace ConverterXML
{
    class Program
    {

        private ParserData parseLine(ParserData pep, ParserDataFamily family, string position, string[] line, string parseType, string rowType)
        {

            int i = 0;
            foreach (string field in parseType.Split(','))
            {
                i++;
                try
                {
                    if (rowType.Contains("P"))
                    {
                        pep.person.Add(field, line[(i)]);
                    }
                    else if (rowType.Contains("A") || rowType.Contains("T") || rowType.Contains("F"))
                    {
                        if (position.Contains("P"))
                        {
                            if (rowType.Contains("A"))
                            {
                                pep.personAddress.Add(field, line[(i)]);
                            }
                            else if (rowType.Contains("T"))
                            {
                                pep.personPhone.Add(field, line[(i)]);
                            }
                        }
                        else if (position.Contains("F"))
                        {
                            if (rowType.Contains("F"))
                            {
                                family.family.Add(field, line[(i)]);
                            }
                            else if (rowType.Contains("A"))
                            {
                                family.familyAddress.Add(field, line[(i)]);
                            }
                            else if (rowType.Contains("T"))
                            {
                                family.familyPhone.Add(field, line[(i)]);
                            }
                        }
                    }
                }
                catch
                {
                    //no value, do nothing...
                }
            }

            return pep;
        }

        static void Main(string[] args)
        {
            Program p = new Program();
            var parseType = new Dictionary<string, string>();
            parseType.Add("P", @"firstname,lastname");
            parseType.Add("A", @"street,city,zipcode");
            parseType.Add("T", @"mobile,landline");
            parseType.Add("F", @"name,born");

            string[] lines = File.ReadAllLines(@"old_data.txt");

            List<ParserData> peoplesParsed = new List<ParserData>();
            ParserData pep = new ParserData();
            ParserDataFamily family = new ParserDataFamily();
            string position = @"";

            foreach (string line in lines)
            {
                string[] lineList = line.Split('|');
                if (lineList[0].Contains("P"))
                {
                    position = @"P";
                    pep = new ParserData();
                    peoplesParsed.Add(pep);
                }
                else if (lineList[0].Contains("F"))
                {
                    position = @"F";
                    family = new ParserDataFamily();
                    pep.personFamily.Add(family);
                }

                p.parseLine(pep, family, position, lineList, parseType[lineList[0]], lineList[0]);

            }

            createXML xml = new createXML();
            var output = xml.getXML(peoplesParsed);

            Console.WriteLine(xml.getXML(peoplesParsed));
            output.Save("new_data.xml");

            Console.ReadKey();
        }
    }


    public class ParserData
    {
        public Dictionary<string, string> person = new Dictionary<string, string>();
        public Dictionary<string, string> personAddress = new Dictionary<string, string>();
        public Dictionary<string, string> personPhone = new Dictionary<string, string>();
        public List<ParserDataFamily> personFamily = new List<ParserDataFamily>();
    }


    public class ParserDataFamily
    {
        public Dictionary<string, string> family = new Dictionary<string, string>();
        public Dictionary<string, string> familyAddress = new Dictionary<string, string>();
        public Dictionary<string, string> familyPhone = new Dictionary<string, string>();
    }


    public class createXML
    {
        public XElement getXML(List<ParserData> personlist)
        {
            var xml = new XElement("people",
                     personlist.Select(person =>
                         new XElement("person",
                             person.person.Select(p => new XElement(p.Key, p.Value)),
                                    new XElement("address", person.personAddress.Select(a => new XElement(a.Key, a.Value))),
                                    new XElement("phone", person.personPhone.Select(t => new XElement(t.Key, t.Value))),
                                    person.personFamily.Select(family =>
                                        new XElement("family",
                                            family.family.Select(f => new XElement(f.Key, f.Value)),
                                            new XElement("address", family.familyAddress.Select(a => new XElement(a.Key, a.Value))),
                                            new XElement("phone", family.familyPhone.Select(t => new XElement(t.Key, t.Value)))
                                        )

                             )
                         ))
                );

            xml.XPathSelectElements("//*[string-length() = 0]").ToList().Remove();
            return xml;
        }
    }

}